// Port to listen requests from
var port = 1711;

// Modules to be used
var express = require('express');
var bodyParser = require('body-parser');
var cookieParser = require('cookie-parser');
var sqlite3 = require('sqlite3').verbose();
var app = express();
var db = new sqlite3.Database('db.sqlite');
app.use(cookieParser())
var token;
// application/x-www-form-urlencoded parser
var urlencodedParser = bodyParser.urlencoded({ extended: false });
// Log requests
app.all("*", urlencodedParser, function(req, res, next){
	console.log(req.method + " " + req.url);
	console.dir(req.headers);
	console.log(req.body);
	console.log();
	next();
});

// Serve static files
app.use(express.static('public'));

app.get("/users", function(req, res, next) {
	

db.all('SELECT * FROM users;', function(err, data) {
		res.json(data);
	});

});


app.post("/login", function(req, res, next) {
  var d = new Date();
  var time = d.getTime(); 
  db.all('SELECT ident, password FROM users WHERE ident=? AND password=?;',[req.body.login,req.body.password], function(err, data) {
    //on vérifie si le compte existe
    if(data.length!=0){
      db.all('SELECT * FROM sessions;', function(err, donne) {
		//le token est composé du nom d'utilisateur plus l'heure en milliseconde de connexion (variable time)        
		var token =req.body.login+time;
        //on vérfie si une session existe déja dans la base pour cet utilisateur
        db.all('SELECT token FROM sessions WHERE ident=?',[req.body.login], function(err,dataa){
         // si c'est la premiere session pour cet utilisateur
          if (dataa.length==0){
            var insertion= db.prepare('INSERT INTO sessions ("ident","token") VALUES (?,?);',[req.body.login,token]);
            res.cookie('session token', 'token: '+token+' ', { maxAge: 900000, httpOnly: true });			
            insertion.run();	

            res.json('status: (true),token: '+token+' ');
          }
          //si une session existe déja dans la base pour cet utilisateur on met le token a jour avec l'heure de connexion
          else{
			//le token est composé du nom d'utilisateur plus l'heure en milliseconde de connexion (variable time)             
			token=req.body.login+time;
            var insertionn= db.prepare('UPDATE sessions SET ident=?, token=? WHERE ident=?',[req.body.login,token,req.body.login]);
            res.cookie('session token', 'token: '+token+' ', { maxAge: 900000, httpOnly: true });			
            insertionn.run();
            res.json('status: (true),token: '+token+' ');
          }
        });
      });
	}
		else{
			res.json('status: (false)');
		}
	});
});

// Startup server
app.listen(port, function () {
	console.log('Le serveur est accessible sur http://localhost:' + port + "/");
	console.log('La liste des utilisateurs est accessible sur http://localhost:' + port + "/users");
});
